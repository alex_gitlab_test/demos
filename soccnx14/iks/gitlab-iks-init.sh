#!/bin/bash
# Maintainer: Nico Meisenzahl <nico@meisenzahl.org>
# Not intended for production!

# Can be used to get needed information to connect IKS with GitLab
# More details: https://gitlab.com/help/user/project/clusters/index.md#installing-applications


ibmcloud ks cluster-config soccnx_cluster > /dev/null 
eval $(ibmcloud ks cluster-config soccnx_cluster |tail -n 1) > /dev/null

# Create needed resources
kubectl create -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: gitlab-managed-apps
spec:
  finalizers:
  - kubernetes
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-ci
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: system:serviceaccount:gitlab-managed-apps:default
---
EOF

# Get values
CLUSTER=$(kubectl config view --minify | grep name | cut -f 2- -d ":" | tr -d " " | head -n 1)
APISERVER=$(kubectl config view --minify | grep server | cut -f 2- -d ":" | tr -d " ")
CAPATH=$(echo "${KUBECONFIG%/*}");CAFILE=$(kubectl config view --minify | grep certificate-authority | cut -f 2- -d ":" | tr -d " ")
TOKEN=$(kubectl describe secret -n gitlab-managed-apps $(kubectl get secrets -n gitlab-managed-apps | grep ^default | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d " ")

echo "Cluster name: "
echo $CLUSTER
echo ""
echo "API URL: " 
echo $APISERVER
echo ""
echo "Token: "
echo $TOKEN
echo ""
echo "CA: "
cat $CAPATH/$CAFILE
echo ""
echo "Namespace:"
echo "gitlab-managed-apps"
