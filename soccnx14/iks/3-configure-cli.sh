#!/bin/bash
# Maintainer: Nico Meisenzahl <nico@meisenzahl.org>
# Not intended for production!

# Disable colors
export BLUEMIX_COLOR=false

# Display cluster info
ibmcloud ks clusters
ibmcloud ks workers soccnx_cluster

# Configure kubectl
ibmcloud ks cluster-config soccnx_cluster
eval $(ibmcloud ks cluster-config soccnx_cluster |tail -n 1)
kubectl get nodes

# helm init
helm init --upgrade
