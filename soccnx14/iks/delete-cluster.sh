#!/bin/bash
# Maintainer: Nico Meisenzahl <nico@meisenzahl.org>
# Not intended for production!

# Disable colors
export BLUEMIX_COLOR=false

# Delete the cluster
yes | ibmcloud ks cluster-rm soccnx_cluster
