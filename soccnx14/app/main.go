// Small hello world web app
// Maintainer: Nico Meisenzahl <nico@meisenzahl.org>
// Not intended for production!

package main

import (
	"io"
	"net/http"
)

var a = "Hello World!"

func hello(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, a)
}

func main() {
	http.HandleFunc("/", hello)
	http.ListenAndServe(":8000", nil)
}
